package study;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WordCount {

	public static void main(String[] args) {
		String inputPath = "D:\\input.txt";
		String outputPath = "D:\\output.txt";
		String content = readText(inputPath);

		int charCount = charCount(content);
		writeText(outputPath, "characters:" + charCount + "\n", true);
		
		int wordCount = wordCount(content);
		writeText(outputPath, "words:" + wordCount + "\n", true);
		
		int rowsCount = rowsCount(inputPath);
		writeText(outputPath, "lines:" + rowsCount + "\n", true);
		
		String englishCount = englishCount(content);
		writeText(outputPath, englishCount + "", true);
	}
	
	//统计文件的字符数
	public static int charCount(String content){
		return content.length();
	}
	
	// （至少以4个英文字母开头）
	public static int wordCount(String content) {
		int count = 0;
		String[] array = { ".", " ", "?", "!" };
		for (int i = 0; i < array.length; i++) {
			content = content.replace(array[i], ",");
		}

		String[] contentArray = content.split(",");
		for (int i = 0; i < contentArray.length; i++) {
			if (contentArray[i].length() >= 4) {
				count++;
			}
		}
		return count;
	}
	
	//统计文件的有效行数
	public static int rowsCount(String filePath) {
		int count = 0;
		try {
			File file = new File(filePath);
			if (file.isFile() && file.exists()) { // 判断文件是否存在
				InputStreamReader read = new InputStreamReader(new FileInputStream(file), "UTF-8");// 考虑到编码格式
				BufferedReader bufferedReader = new BufferedReader(read);
				
				StringBuffer sb = new StringBuffer();
                String lineTxt = null;
                while((lineTxt = bufferedReader.readLine()) != null){
                    sb.append(lineTxt).append("\r\n");
                    count ++;
                }
				read.close();
			} else {
				System.out.println("rowsCount方法提示：找不到指定的文件.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}
	
	
	//统计文件中各单词的出现次数
	public static String englishCount(String content) {
		String result = "";
		// 找出所有的单词
		String[] array = { ".", " ", "?", "!" };
		for (int i = 0; i < array.length; i++) {
			content = content.replace(array[i], ",");
		}
		String[] contentArray = content.split(",");
		// 遍历 记录
		Map<String, Integer> map = new HashMap<String, Integer>();
		for (int i = 0; i < contentArray.length; i++) {
			String key = contentArray[i];
			// 转为小写
			String key_l = key.toLowerCase();
			if (!"".equals(key_l) && key_l.length()>=4) {
				Integer num = map.get(key_l);
				if (num == null || num == 0) {
					map.put(key_l, 1);
				} else if (num > 0) {
					map.put(key_l, num + 1);
				}
			}
		}
		
		//按出现次数排序
		List<Map.Entry<String, Integer>> info = new ArrayList<Map.Entry<String, Integer>>(
				map.entrySet());
		Collections.sort(info, new Comparator<Map.Entry<String, Integer>>() {
			public int compare(Map.Entry<String, Integer> obj1,
					Map.Entry<String, Integer> obj2) {
				return obj2.getValue() - obj1.getValue();
			}
		});
		//最多输出 10行
		int length = 10;
		if(info.size() <= 10){
			length = info.size();
		}
		for (int j = 0; j < length; j++) {
            result = result + info.get(j).getKey() + ":" + info.get(j).getValue() + "\n";
        }
		return result;
	}
	

	//读取文档内容
	public static String readText(String filePath) {
		String textStr = "";
		try {
			File file = new File(filePath);
			if (file.isFile() && file.exists()) { // 判断文件是否存在
				InputStreamReader read = new InputStreamReader(new FileInputStream(file), "UTF-8");// 考虑到编码格式
				BufferedReader bufferedReader = new BufferedReader(read);
				
				StringBuffer sb = new StringBuffer();
                String lineTxt = null;
                while((lineTxt = bufferedReader.readLine()) != null){
                    sb.append(lineTxt).append("\r\n");
                }
				read.close();
				textStr = sb.toString();
				//System.out.println(textStr);
			} else {
				System.out.println("readText方法提示: 找不到指定的文件.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return textStr;
	}
	
	//将结果写入文档
	public static void writeText(String filePath, String content, boolean flag) {
		try {
			File file = new File(filePath);
			if (file.exists()) {
				FileWriter fw = new FileWriter(file, flag); // flag=true 为追加模式
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write(content);
				bw.flush();
				bw.close();
				fw.close();
			}else{
				System.out.println("writeText方法提示: 找不到指定的文件.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
